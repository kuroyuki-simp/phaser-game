import 'phaser';

export default class Menu extends Phaser.Scene
{
    constructor ()
    {
        super('menu');
    }

    preload ()
    {
        this.load.image('logo', 'assets/phaser3-logo.png');
        this.load.image('libs', 'assets/libs.png');
        this.load.glsl('bundle', 'assets/plasma-bundle.glsl.js');
        this.load.glsl('stars', 'assets/starfields.glsl.js');
    }

    create ()
    {
        // this.add.shader('RGB Shift Field', 0, 0, 800, 600).setOrigin(0);

        

        const logo = this.add.image(400, 250, 'logo');

        this.tweens.add({
            targets: logo,
            y: 350,
            duration: 1500,
            ease: 'Sine.inOut',
            yoyo: true,
            repeat: -1
        })

        this.input.on('pointerup', function (pointer) {

            this.scene.start('menu2');

        }, this);

    }
}

const config = {
    type: Phaser.AUTO,
    backgroundColor: 0x00FFFFFF,
    width: 800,
    height: 600,
    scene: [Menu]
};

const game = new Phaser.Game(config);
